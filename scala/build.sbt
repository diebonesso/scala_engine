name := "scala-engine"
scalaVersion := "2.11.7"
scalacOptions := Seq(
  "-deprecation",
  "-unchecked",
  "-encoding",
  "utf8",
  "-Xlint"
)
version := "0.1"

val sparkVersion = "2.4.0"
val circeVersion = "0.7.0"

libraryDependencies ++= Seq(
  "org.apache.spark" %% "spark-core" % sparkVersion,
  "org.apache.spark" %% "spark-sql" % sparkVersion,
  "org.apache.spark" %% "spark-mllib" % sparkVersion,
  "org.apache.spark" %% "spark-streaming" % sparkVersion,
  "org.apache.spark" %% "spark-hive" % sparkVersion,
  "org.mongodb" %% "casbah" % "2.8.1",
  "org.apache.kafka" %% "kafka" % "2.3.0",
  "org.scalatest" %% "scalatest" % "3.0.5" % "test",
  "io.circe" %% "circe-core" % circeVersion,
  "io.circe" %% "circe-generic" % circeVersion,
  "io.circe" %% "circe-parser" % circeVersion
)
