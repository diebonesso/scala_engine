package utils.db
import Config.DBConfig
import Models.NewsMeta
import com.mongodb.casbah.MongoClient
import com.mongodb.casbah.commons.MongoDBObject

object Database {

  lazy val newsCollection = getCollection(DBConfig.news)

  def getCollection(collectionName: String) = {
    val mongoClient = MongoClient(DBConfig.dbHost, DBConfig.dbPort)
    val db = mongoClient(DBConfig.dbName)
    db(collectionName)
  }

  def insertNews(newsMeta: NewsMeta) = {
    val attributes = Map(
      "eventId" -> newsMeta.eventId,
      "city" -> newsMeta.city,
      "activeTime" -> newsMeta.activeTime,
      "url" -> newsMeta.url,
      "referrerHostClass" -> newsMeta.referrerHostClass,
      "region" -> newsMeta.referrerHostClass,
      "time" -> newsMeta.time,
      "userId" -> newsMeta.userId,
      "sessionStart" -> newsMeta.sessionStart,
      "deviceType" -> newsMeta.deviceType,
      "sessionStop" -> newsMeta.sessionStop,
      "country" -> newsMeta.country,
      "os" -> newsMeta.os
    )
    val uo = MongoDBObject.newBuilder
    uo ++= attributes
    val doc = uo.result
    // println(s"${restaurant.id}: ${restaurant.name}")
    newsCollection.insert(doc)
  }
}
