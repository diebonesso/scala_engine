package Models
case class NewsMeta(
                     var eventId: Int,
                     var city: String,
                     var activeTime: Int,
                     var url: String,
                     var referrerHostClass : String,
                     var region: String,
                     var time: Long,
                     var userId: String,
                     var sessionStart: Boolean,
                     var deviceType: String,
                     var sessionStop: Boolean,
                     var country: String,
                     var os: String
                   )

