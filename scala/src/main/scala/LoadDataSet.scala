import Models.NewsMeta
import java.io.File

import scala.io.Source
import io.circe.parser
import io.circe.generic.semiauto.deriveDecoder
import utils.db.Database

object LoadDataSet {

  def LoadDataSet(): Unit = {}

  def Load(filename: String): Unit = {

    val file = new File("in", filename).getPath
    val src = Source.fromFile(file)
    val lIter = src.getLines
    implicit val staffDecoder = deriveDecoder[NewsMeta]
    while (lIter.hasNext) {
      val l = lIter.next()
      val decodeResult = parser.decode[NewsMeta](l)
      decodeResult match {
        case Right(newsMeta) => addToEntries(newsMeta)
        case Left(error)     => println("profile")
      }
    }
  }

  def addToEntries(currentItem: NewsMeta) = {
    Database.insertNews(currentItem)
    println(currentItem)
  }

}
