package Config

object DBConfig {
  val dbName = "data"
  val news = "news"
  val sessions = "sessions"
  val dbHost = "localhost"
  val dbPort = 27017
}
