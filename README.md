
# Scala engine for recomendation

 1 - Install docker [here](https://docs.docker.com/install/linux/docker-ce/ubuntu/)
  
 2 - Test to make sure things are working properly by launching a Dockerized web server:
 
 ```
 λ docker run hello-world
 ```
3 - Build docker image for this project. Go inside scala_engine directory
 ```
 λ docker build -t scala_engine .
 ```
4 - We can check the new image 'scala_engine' with the docker command below:
```
 λ docker images
```
Output :

```
REPOSITORY                 TAG                 IMAGE ID            CREATED             SIZE
scala_engine               latest              2c777ae5eb7b        9 minutes ago       93.7MB
```

5 - init a docker swarm cluster and listens on localhost
```
 λ docker swarm init --advertise-addr 127.0.0.1
```

6- create an overlay network
```
docker network create --driver overlay swarm-net
```

7- Docker run

```
λ  docker run -it --name scala_engine --rm -p 9078:9078 -p 8088:8088 scala_engine
```

References
* https://www.linkedin.com/pulse/brincando-de-hadoop-hdfs-yarn-com-o-docker-marcelo-marques/

* https://towardsdatascience.com/product-recommender-using-amazon-review-dataset-e69d479d81dd

* https://github.com/gabrielspmoreira/chameleon_recsys

* https://www.linkedin.com/pulse/spark-streaming-vs-flink-storm-kafka-streams-samza-choose-prakash/

* https://blog.newnius.com/how-to-quickly-setup-a-hadoop-cluster-in-docker.html

* https://hub.docker.com/r/lewuathe/hadoop-base/dockerfile

* https://github.com/sequenceiq/docker-pam/blob/master/ubuntu-14.04/Dockerfile 

* https://github.com/Lewuathe/docker-hadoop-cluster

* https://github.com/big-data-europe/docker-hadoop

Problem on hadoop sh

https://issues.apache.org/jira/browse/HADOOP-16167

Data from drive

https://www.matthuisman.nz/2019/01/download-google-drive-files-wget-curl.html

Engine
http://ampcamp.berkeley.edu/big-data-mini-course/movie-recommendation-with-mllib.html

